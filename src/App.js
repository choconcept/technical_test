import './App.css';
import  {useEffect, useState} from 'react';
import Form  from './Form';
import Table  from './Table';


 function App() {
  const [infos, updateInfos] = useState({
    width: "",
    height: "",
    color: "",
  });
  const [square, setSquare] = useState([]);
  const [visited, setVisited] = useState([]);
  const [result, setResult] = useState([]);

  const [count, setCount] = useState(0);

  const handleSquare = (squareInfo,colorList) => {
    
    let copy =Array.from({length: squareInfo.height},()=> Array.from({length: squareInfo.width}, () => colorList[Math.floor(Math.random() * colorList.length)]));
    setSquare(copy)
    copy=Array.from({length: squareInfo.height},()=> Array.from({length: squareInfo.width}, () => 0))
    setVisited(copy)
     setResult(copy)
     updateInfos(squareInfo)
   
    //
    
  

  };
  useEffect(()=>{

    let current_max = 0;
     for (let i = 0; i < infos.height; i++) {
        for (let j = 0; j < infos.width; j++) {
            let  copy =Array.from({length: infos.height},()=> Array.from({length: infos.width}, () => 0));
             setVisited(copy);
             setCount(0);
 
            if (j + 1 < infos.width)
                BFS(square[i][j], square[i][j + 1], i, j);
 
            if (count >= current_max) {
                current_max = count;
                reset_result(square[i][j]);
            }
              copy =Array.from({length: infos.height},()=> Array.from({length: infos.width}, () => 0))
               setVisited(copy);
               setCount(0);
 
            if (i + 1 < infos.height)
                BFS(square[i][j], square[i + 1][j], i, j);
 
            if (count >= current_max) {
                current_max = count;
                reset_result(square[i][j]);
            }
        }
    }

  }, [square])
  
  const  is_valid=( x,y,key)=>
  {
      if (x < infos.height && y < infos.width && x >= 0 && y >= 0) {
          if (visited[x][y] == false && square[x][y] == key)
              return true;
          else
              return false;
      }
      else
          return false;
  }
  const reset_result=(key)=>
{
    for (let i = 0; i < infos.height; i++) {
        for (let j = 0; j < infos.width; j++) {
            if (visited[i][j] && square[i][j] == key){
               let inter = result
               inter[i][j] = visited[i][j];
               setResult(inter)
            }
                
            else
                result[i][j] = 0;
        }
    }
}

  const BFS=( x,  y,  i,  j)=>{
    if (x != y)
        return;
    let inter = visited
    inter[i][j] = 1;
     setVisited(inter)
     setCount(count +1)

    let x_move = [ 0, 0, 1, -1 ];
    let y_move = [ 1, -1, 0, 0 ];
 
    for (let u = 0; u < 4; u++){
      if (is_valid(i + y_move[u], j + x_move[u], x)){
         BFS(x, y, i + y_move[u], j + x_move[u]);

      }

    }
        

}
    
  return (
    
    <div className="App">
        <Form handleSquare={handleSquare} />
        <Table infos={infos} square={square} />      
      
    </div>
  );
}

export default App;
 


