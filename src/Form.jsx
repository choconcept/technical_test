import { useState } from "react";
export default function Form({handleSquare}) {
    const [squareInfo, setSquareInfo] = useState({
        width: "",
        height: "",
        color: "",
      });
       const handleChange = (event) => {
        setSquareInfo({ ...squareInfo, [event.target.name]: event.target.value });
      };
      const handleSubmit = (event) => {
        event.preventDefault();
        let colorList = []
        for(let i = 0; i <  squareInfo.color  ; i++)
        colorList.push(Math.floor(Math.random()*16777215).toString(16));
         handleSquare(squareInfo,colorList);
         setSquareInfo({ width: "", height: "", color: "" });

      };

    return (
      <div>
        <form onSubmit={handleSubmit}> 
          <div>
            <h3>Form</h3>
          </div>
          <div>
            <input
              type="number"
              name="width"
              placeholder="Width"
              value={squareInfo.width}
              onChange={(e)=> {handleChange(e)}}
            />
          </div>
          <div>
            <input
              type="number"
              name="height"
              placeholder="Height"
              value={squareInfo.height}
              onChange={(e)=> {handleChange(e)}}
            />
          </div>
          <div>
            <input
              type="number"
              name="color"
              placeholder="Color"
              value={squareInfo.color}
              onChange={(e)=> {handleChange(e)}}
            />
          </div>
          <div>
            <button>Submit</button>
          </div>
        </form>
      </div>
    );
  }